import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ProductsService } from '../../services/products.service';
import { Router } from '@angular/router';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  productsLst : Product[];
  constructor(private auth: AuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.productsLst = JSON.parse(localStorage.getItem('prod_lst')); 
  }

  newProduct(): void{
    this.router.navigateByUrl('/pcreate');
  }

  logout() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
