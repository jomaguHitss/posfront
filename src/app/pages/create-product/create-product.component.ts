import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { NgForm } from '@angular/forms';
import { ProductsService } from 'src/app/services/products.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  product: Product = new Product();

  constructor(private auth:AuthService,
              private productsService: ProductsService,
              private router: Router) { }

  ngOnInit() {
    this.product.name = '';
    this.product.description = '';
    this.product.price = '';
  }

  createProduct(form: NgForm){
    if (  form.invalid ) { return; }
    
    Swal.fire({
      allowOutsideClick: false,
      type: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();
    
    this.productsService.createProduct(this.product)
    .subscribe((res:any) => {
      Swal.close();
      this.productsService.getProducts();
    }, (err) => {
      console.log(err.error.error.message);
      Swal.fire({
        type: 'error',
        title: 'Error al autenticar',
        text: err.error.error.message
      });
    })
  }

  logout() {
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }

}
