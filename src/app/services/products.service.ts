import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  sessionData: any;
  private API_PRODUCTS = environment.apiProducts;

  constructor(public router: Router,
              private http: HttpClient) {}

  public getProducts(){
    let body = {};

    return this.http.get(this.API_PRODUCTS)
    .pipe(
      map((res:any) => {
        try{
          if(res != null)
            localStorage.setItem('prod_lst', JSON.stringify(res));
          else
            localStorage.setItem('prod_lst', JSON.stringify('[]'));
          this.router.navigate(['/products']);
        } catch(error){
          console.log(error);
        }
      }
    ))
    .subscribe((res:any) => {
    }, (err:HttpErrorResponse) => {
        console.log(err.message);
    });
  }
  
  public createProduct( product: Product ) {

    let body = {
      idProduct: 0,
      name: product.name,
      description: product.description,
      price: product.price
    };

    let headers = new HttpHeaders({
        'Accept':'application/json',
        'Content-Type':'application/json',
        'Access-Control-Allow-Origin':'*'
    });

    return this.http.post(this.API_PRODUCTS,  body, { headers } )
    .pipe(
      map( resp => {
        if(resp != null){
          return resp;
        }
      })
    );
  }
}
