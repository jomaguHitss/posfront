# POS FRONT END

_Sistema en Angular 7, con pagina de Login consumiendo desde Firebase, y pagina Listado de Productos consumiendo desde un API RESTfull en JavaEE._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Angular 7 o superior_
_Node.js_
_IDE(recomendado): Visual Studio Code_


### Instalación 🔧

_descargar el proyecto_
_copiar en el directorio deseado_
_entrar a ventana de comandos y dirigirse a la raiz del proyecto_
_ejecutar: npm install_
_ejecutar:ng serve -o, para levantar el servicio_


## Ejecutando las pruebas ⚙️

_Usuarios registrados en Firebase para probar Login:_
_Usuario 1_
_usuario: mauricio.guzmanjc@gmail.com_
_contraseña:123456_

_Usuario 2_
_usuario: testing@edimca.com_
_contraseña:123456_

_Extension Chrome Cors usada_
_EASY CORS_

![](https://github.com/Klerith/angular-login-demoapp/blob/master/src/assets/images/demo.png?raw=true)


## Construido con 🛠️

* [Angular](https://angular.io/) - El framework front usado
* [Node.js](https://nodejs.org/es/) - Servicio levantar sistema front
* [Bootstrap](https://getbootstrap.com/) - template para estilos


## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

---
⌨️ con ❤️ por Mauricio Guzman(https://github.com/MauricioHub) 😊
